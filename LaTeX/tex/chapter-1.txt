% 3
     I tremble to the depths of my soul and ask my dæmon: "Why this cup
     to me?"
                                                               --WAGNER.

     Life at last has found a meaning.
                                       --WAGNER: Letter to Frau Wille.


Reference has already been made to the fact that Beethoven's opus 1 was
published in 1795, something like three years after taking up his
residence in Vienna, and when he was twenty-four years of age. It
consists of three Trios for piano and strings. When Haydn returned from
London and heard these Trios, the master criticised one of them and
advised him not to publish it. Beethoven thought this particular one the
best of the three, and others concur with him in this opinion. Shortly
after, he published his opus 2, consisting of three sonatas dedicated to
Haydn, besides variations and smaller pieces. But this does not by any
means give the amount of his compositions for this period, some of which
were not published until many years afterward.

All this time, Beethoven, though playing frequently at the houses of his
aristocratic friends, had not yet made his appearance in public, but
about the time that his opus 1 appeared, he played at a concert given
in aid of the Widow's Fund of the Artists' Society. He composed for this
occasion a Grand Concerto (opus 15) in C major for piano and orchestra,
taking the piano part himself. It was finished on the day preceding that
on which the concert was held, the copyists waiting in another room for
their parts. At the rehearsal, the piano being one-half note out of
tune, he transposed it into C sharp, playing it without the notes. Very
soon after, he appeared again in public, at a concert given for the
benefit of Mozart's widow, when he played one of Mozart's concertos. The
beginning once made, he appeared rather frequently as a performer, not
only in Vienna, but extended his trips the next year as far as Berlin,
where he encountered Hümmel.

But Beethoven's mind was always turned toward composition. It had been
the aim of his life, even at Bonn, to become a great creative artist.
For this he had left his native city, and the larger opportunities for
musical culture afforded by his life in Vienna must have directed his
thoughts still more strongly into this channel. An important social
event of the period was the annual ball of the Artists' Society of
Vienna. Süsmayer, pupil and intimate friend of Mozart, the composer of
several of the "Mozart Masses," had composed music for this ball and
Beethoven was asked to contribute something likewise, with the result
that he composed twelve waltzes and twelve minuets for it. He also had
in hand at the same time piano music, songs, and studies in orchestral
composition. Nothing which he produced in these years, however, gave any
forecast of what he would eventually attain to. This is paralleled in
the case of Bach, who, up to his thirtieth year was more famous as a
performer than as composer.

Beethoven's earlier compositions were regarded as the clever product of
an ambitious young musician. Although later in life, he all but
repudiated the published work of these years, some of the thoughts from
the sketch-books of this period were utilized in the work of his best
years.

He acquired a habit early in life of carrying a note-book when away from
his rooms, in which he recorded musical ideas as they came to him. His
brain teemed with them; these he entered indiscriminately, good and bad,
assorting them later, discarding some, altering others, seldom retaining
a musical thought exactly as it was first presented to his
consciousness. Music became the one absorbing passion of his life. It
took the place of wife and children; it was of more importance to him
than home or any other consideration. His compositions show continual
progress toward artistic perfection to the end of his life, and this was
attained only by infinite labor.

It may not be out of place here to reflect on the essentially unselfish
character of the man of genius. He lives and strives, not for himself,
but for others; he pursues an objective end only. Among the forces
making for the regeneration of mankind, he is foremost.

There is little of importance to record concerning Beethoven for the few
years following the publication of his opus 1. He continued to perform
occasionally in public, and also gave a few lessons, but his time was
taken up with study and composition for the most part. It was a period
of earnest endeavor, the compositions of which consist of the better
class of piano music, as well as trios, quartets and occasional songs,
his work being much in the style of Mozart and Haydn; the quality of
emotional power and intellectuality not yet having appeared to any
extent.

His great productions, those that show his genius well developed, are
coincident with the beginning of the nineteenth century. The years 1800
and 1801 were an epoch with him as a composer. He was now thirty, and
was beginning to show of what stuff he was made. These two years saw the
production of some of the imperishable works of the master, namely: the
First Symphony, the Oratorio Christus am Oelberg, and the Prometheus
Ballet Music. It is probable that he had given earnest thought to these
works for some years previously, and had had them in hand for two years
or more before their appearance. The First Symphony calls for special
mention as in it the future Symphonist is already foreshadowed. He was
almost a beginner at orchestral work, but it marks an epoch in this
class of composition, raising it far beyond anything of the kind that
had yet appeared. Viewed in the light of later ones it is apparent that
he held himself in; that he was tentative compared with his subsequent
ones. Considered as a symphony and compared with what had been produced
in this class up to that time, it is a daring innovation and was
regarded as such by the critics. He broadened and enlarged the form and
gave it a dignity that was unknown to it before this time.

Beethoven's sonatas are as superior to those that had preceded them as
are his symphonies. He enlarged them, developed the Scherzo from the
Minuet and made them of more importance in every way. With Haydn the
Minuet was gay and lively, a style of music well adapted to Haydn's
particular temperament and character; but Beethoven in the Scherzo
carried the idea further than anything of which Haydn had dreamed.
Before Beethoven's First Symphony appeared, he had composed a dozen or
more sonatas and was in a position to profit by the experience gained
thereby. He felt his way in these, the innovations all turning out to be
improvements.

One has only to compare the sonatas of Mozart and Haydn with those of
Beethoven to be at once impressed with the enormous importance of the
latter. As has been stated, the experience gained with the sonata was
utilized in the First Symphony, each succeeding one showing growth.
Beethoven's artistic instinct was correct, but he did not trust to this
alone. He proceeded carefully, weighing the matter well, and his
judgment was usually right. There is evidence from his exercise books
that he had this Symphony in mind as early as 1795. It was first
produced on April 2, 1800, at a concert which he gave for his own
benefit at the Burg theatre. On this occasion he improvised on the theme
of the Austrian National Hymn, recently composed by Haydn, well known in
this country through its insertion in the Hymnal of the Protestant
Episcopal Church, under the title of Austria. Beethoven's hearing was
sufficiently intact at this time to enable him to hear his symphonies
performed, an important matter while his judgment was being formed.

The Prometheus Ballet Music, opus 43, consisting of overture,
introduction and sixteen numbers, was first performed early in 1801, and
achieved immediate success, so much so that it was published at once as
pianoforte music. In addition to the Prometheus, there is to be
credited to this period the C minor concerto, opus 37, a septet for
strings and wind, opus 20, a number of quartets, and other compositions.
The Christus am Oelberg (The Mount of Olives), opus 85, Beethoven's
first great choral work, has already been mentioned. In this oratorio
Jesus appears as one of the characters, for which he has been severely
criticised. His judgment was at fault in another respect also in having
the concert stage too much in mind. The composition at times is operatic
in character, while the text calls for a mode of treatment solemn and
religious, as in Passion-music. If set to some other text, this work
would be well nigh faultless; the recitatives are singularly good, and
there is a rich orchestration. It is reminiscent of Händel and prophetic
of Wagner. The Hallelujah Chorus in particular is a magnificent piece of
work. As is the case with the Messiah, its beauties as well as its
defects are so apparent, so pronounced, that the latter serve as a foil
to bring out its good qualities in the strongest relief. It was first
performed in the spring of 1803, in Vienna, on which occasion Beethoven
played some of his other compositions. It was repeated three times
within the year.

Other contributions of 1801 are two grand sonatas, the "Pastorale" in D,
opus 28, the Andante of which is said to have been a favorite of
Beethoven's and was often played by him, and the one in A flat, opus 26,
dedicated to Prince Karl Lichnowsky and containing a grand funeral
march. Then there are the sonatas in E flat and C sharp minor, published
together as opus 27, and designated Quasi una Fantasia. The latter is
famous as the "Moonlight" sonata, dedicated to Julia Guicciardi.
Neither of these names were authorized by Beethoven. Besides these,
there are the two violin sonatas, A minor, and F, dedicated to Count
Fries, and lesser compositions. The Second Symphony (in D) is the chief
production of 1802. In addition there are the two piano sonatas in G,
and D minor, opus 31, and three sonatas for violin and piano, opus 30,
the latter dedicated to the Emperor of Russia. They form a striking
example of Beethoven's originality and the force of his genius, and must
have been caviar to his public.

The Second Symphony is a great advance on the first, and consequently a
greater departure from the advice laid down to him by others. His
independence and absolute faith in himself and the soundness of his
judgment are clearly illustrated here. The composition is genial and in
marked contrast to the gloomy forebodings that filled his mind at this
time. The second movement, the Larghetto, is interesting on account of
the introduction of conversation among the groups of instruments, an
innovation which he exploited to a much greater extent in subsequent
works. In the Larghetto one group occasionally interrupts the other,
giving it piquancy. There is a rhythm and swing to it which makes it the
most enjoyable of the four movements. The critics hacked it again as
might have been expected, the result being that the next one diverged
still more from their idea of what a good symphony should be.

It was at this period that life's tragedy began to press down on him. He
had left youth behind, and had entered on a glorious manhood. He was the
idol of his friends, although his fame as a great composer had yet to
be established. The affirmations of his genius were plainly apparent to
him, if not to others, and he knew that he was on the threshold of
creating imperishable masterpieces. A great future was opening out
before him, which, however, was in great part to be nullified by his
approaching deafness and other physical ailments. His letters at this
time to his friend Dr. Wegeler, at Bonn, and to others, are full of
misgivings.

But not alone is this unhappy frame of mind to be attributed to
approaching deafness or any mere physical ailment. The psychological
element also enters into the account and largely dominates it. The
extraordinary character of the First and Second Symphonies seem to have
had a powerful effect on his trend of thought making him introspective
and morbidly conscientious. In a mind constituted as was his, it is
quite within bounds to assume that the revelation of his genius was
largely the cause of the morbid self-consciousness which appears in his
letters of the period, and in the "Will." He recognized to the full how
greatly superior this work was to anything of the kind that had yet
appeared; singularly the knowledge made him humble. What he had
accomplished thus far was only an earnest of the great work he was
capable of, but to achieve it meant a surrender of nearly all the ties
that bound him to life. The human qualities in him rebelled at the
prospect. With the clairvoyance superinduced by much self-examination,
he was able to forecast the vast scope of his powers, and the task that
was set him. The whole future of the unapproachable artist that he was
destined to become, was mirrored out to him almost at the beginning of
his career, but he saw it only with apprehension and dread. There were
periods when a narrower destiny would have pleased him more. "Unto
whomsoever much is given, of him shall much be required." He at times
recoiled from the task, and would have preferred death instead. This was
probably the most unhappy period of his life. He had yet to learn the
hardest lesson of all, resignation, renunciation. That harsh mandate
enunciated by Goethe in Faust: "Entbären sollst du, sollst entbären,"
had been thrust on him with a force not to be gainsaid or evaded.

With such a man but one issue to the conflict was possible: obedience to
the higher law. In a conversation held with his friend Krumpholz, he
expressed doubts as to the value of his work hitherto. "From now on I
shall strike out on a new road," he said. He is now dominated by a
greater seriousness; his mission has been shown him. Adieu now to the
light-hearted mode of life characteristic of his friends and of the
time. His new road led him into regions where they could not follow;
from now on he was more and more unlike his fellows, more misunderstood,
isolated, a prophet in the wilderness. Placed here by Providence
specially for a unique work, he at first does not seem to have
understood it in this light, and reached out, the spirit of the man,
after happiness, occasional glimpses of which came to him, as it does to
all sooner or later. He soon found, however, that happiness was not
intended for him, or rather, that he was not intended for it. Something
higher and better he could have, but not this. On coming to Vienna, and
while living with Prince Lichnowsky, he made so much of a concession to
public opinion as to buy a court suit, and he even took dancing
lessons, but he never learned dancing, never even learned how to wear
the court suit properly, and soon gave up both in disgust. The principle
on which he now conducted his life was to give his genius full play, to
obey its every mandate, to allow no obstacle to come in the way of its
fullest development. That this idea controlled him throughout life, is
apparent in many ways, but most of all in his journal. "Make once more
the sacrifice of all the petty necessities of life for the glory of thy
art. God before all," he wrote in 1818, when beginning the Mass in D.
All sorts of circumstances and influences were required to isolate him
from the world to enable him the better to do his appointed work.
Probably no other musician ever made so complete a surrender of all
impedimenta for the sake of his art as did Beethoven.

Music as an art does not conduce to renunciation, since its outward
expression always partakes more or less of the nature of a festival. The
claims of society come more insistently into the life of the musician
than in that of other art-workers, the painter or literary man, for
instance, whose work is completed in the isolation of his study. The
musician, on the contrary, completes his work on the stage. He must
participate in its rendering. He is, more than any other, beset by
social obligations; he perforce becomes to a certain extent gregarious,
all of which has a tendency to dissipate time and energy. It is only by
a great effort that he can isolate himself; that he can retain his
individuality. Beethoven's reward on these lines was great in proportion
to his victory over himself.